const request = require('supertest');
const app = require('./app');

describe('security', () => {
    it('Dummy security test. Request to /, should return 200 with Hello World! message', async () => {
        const res = await request(app)
            .get('/')
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Hello World!');
    });
});
